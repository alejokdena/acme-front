import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  API_URL_REPORT: string = "http://127.0.0.1:8000/api/getReport";
  API_URL_DRIVER: string = "http://127.0.0.1:8000/api/createDriver";
  API_URL_OWNER: string = "http://127.0.0.1:8000/api/createOwner";
  API_URL_VEHICLE: string = "http://127.0.0.1:8000/api/createVehicle";
  API_URL_DRIVERS: string = "http://127.0.0.1:8000/api/getDrivers";
  API_URL_OWNERS: string = "http://127.0.0.1:8000/api/getOwners";
  constructor(private httpCilent: HttpClient) { }

  getReport(): Observable<any>{
    return this.httpCilent.get(this.API_URL_REPORT).pipe(res => res);
  }
  getDrivers(): Observable<any>{
    return this.httpCilent.get(this.API_URL_DRIVERS).pipe(res => res);
  }
  getOwners(): Observable<any>{
    return this.httpCilent.get(this.API_URL_OWNERS).pipe(res => res);
  }
  createDriver(data: any): Observable<any>{
    return this.httpCilent.post(this.API_URL_DRIVER, data).pipe(res => res);
  }
  createOwner(data: any): Observable<any>{
    return this.httpCilent.post(this.API_URL_OWNER, data).pipe(res => res);
  }
  createVehicle(data: any): Observable<any>{
    return this.httpCilent.post(this.API_URL_VEHICLE, data).pipe(res => res);
  }
}
