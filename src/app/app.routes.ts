import { Routes } from '@angular/router';
import { OwnerComponent } from './owner/owner.component';
import { DriverComponent } from './driver/driver.component';
import { VehicleComponent } from './vehicle/vehicle.component';
import { ReportComponent } from './report/report.component';

export const routes: Routes = [
    {path: 'owner', component: OwnerComponent },
    {path: 'driver', component: DriverComponent },
    {path: 'vehicle', component: VehicleComponent },
    {path: 'report', component: ReportComponent },
];
