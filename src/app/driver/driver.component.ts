import { Component } from '@angular/core';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppService } from '../services/app.service';

@Component({
  selector: 'app-driver',
  standalone: true,
  imports: [FormsModule, ReactiveFormsModule],
  templateUrl: './driver.component.html',
  styleUrl: './driver.component.css'
})
export class DriverComponent {
  form: FormGroup = this.fb.group({
    name: [''],
    lastName: [''],
    identification: [''],
    city: [''],
    phone: [''],
    address: [''],    
  });
  constructor(private appService: AppService, private fb: FormBuilder) {}
  driver: any = {
    name: '',
    lastName: '',
    identification: '',
    city: '',
    phone: '',
    address: '',
  };
    onSubmit(){
      this.appService.createDriver(this.driver).subscribe({
        next: (result) => {
          alert("Registro creado exitosamente");
          this.form.reset();
          console.log(this.driver);
        },
        error: (err)=>{
          console.log(err);
        }
      })
    
  }
}
