import { Component } from '@angular/core';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppService } from '../services/app.service';

@Component({
  selector: 'app-owner',
  standalone: true,
  imports: [FormsModule, ReactiveFormsModule],
  templateUrl: './owner.component.html',
  styleUrl: './owner.component.css'
})
export class OwnerComponent {
  form: FormGroup = this.fb.group({
    name: [''],
    lastName: [''],
    identification: [''],
    city: [''],
    phone: [''],
    address: [''],    
  });
  constructor(private appService: AppService, private fb: FormBuilder) {}
  owner: any = {
    name: '',
    lastName: '',
    identification: '',
    city: '',
    phone: '',
    address: '',
  };
    onSubmit(){
      this.appService.createOwner(this.owner).subscribe({
        next: (result) => {
          alert("Registro creado exitosamente");
          this.form.reset();
          console.log(this.owner);
        },
        error: (err)=>{
          console.log(err);
        }
      })
    
  }
}
