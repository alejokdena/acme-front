import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppService } from '../services/app.service';
import { OwnerInterface } from '../interfaces/owner.interface';
import { DriverInterface } from '../interfaces/driver.interface';

@Component({
  selector: 'app-vehicle',
  standalone: true,
  imports: [FormsModule, ReactiveFormsModule],
  templateUrl: './vehicle.component.html',
  styleUrl: './vehicle.component.css'
})
export class VehicleComponent implements OnInit {

  ownerList:OwnerInterface[]= [];
  driverList:DriverInterface[]= [];
  form: FormGroup = this.fb.group({
    plate: [''],
    color: [''],
    brand: [''],
    type: [''],
    fk_driver_id: [''],
    fk_owner_id: [''],    
  });
  constructor(private appService: AppService, private fb: FormBuilder) {}
  ngOnInit(): void {
    this.getOwners();
    this.getDrivers();
  }
  vehicle: any = {
    plate: '',
    color: '',
    brand: '',
    type: '',
    fk_driver_id: '',
    fk_owner_id: '',
  };
  getOwners(){
    this.appService.getOwners().subscribe({
      next: (result) => {
        this.ownerList = result.owners;
        console.log(this.ownerList);
      },
      error: (err)=>{
        console.log(err);
      }
    })
  }
  getDrivers(){
    this.appService.getDrivers().subscribe({
      next: (result) => {
        this.driverList = result.drivers;
        console.log(this.driverList);
      },
      error: (err)=>{
        console.log(err);
      }
    })
  }
    onSubmit(){
      this.appService.createVehicle(this.vehicle).subscribe({
        next: (result) => {
          alert("Registro creado exitosamente");
          this.form.reset();
          console.log(this.vehicle);
        },
        error: (err)=>{
          console.log(err);
        }
      })
  }
}
