export interface OwnerInterface{
    id: number;
    name: string;
    lastName: string;
}