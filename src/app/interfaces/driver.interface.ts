export interface DriverInterface{
    id: number;
    name: string;
    lastName: string;
}