export interface ReportInterface{
    plate: string;
    brand: string;
    driverName: string;
    driverLastName: string;
    ownerName: string;
    ownerLastName:string;
}