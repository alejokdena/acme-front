import { Component, OnInit } from '@angular/core';
import { AppService } from '../services/app.service';
import { ReportInterface } from '../interfaces/report.interface';

@Component({
  selector: 'app-report',
  standalone: true,
  imports: [],
  templateUrl: './report.component.html',
  styleUrl: './report.component.css'
})
export class ReportComponent implements OnInit {

  reportList:ReportInterface[]= [];
  constructor(private appService: AppService) {}
  ngOnInit(): void {
    this.getReport();
  }

  getReport(){
    this.appService.getReport().subscribe({
      next: (result) => {
        this.reportList = result.report;
      },
      error: (err)=>{
        console.log(err);
      }
    })
  }

}
